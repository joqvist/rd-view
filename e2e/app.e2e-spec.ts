import { RdViewPage } from './app.po';

describe('rd-view App', () => {
  let page: RdViewPage;

  beforeEach(() => {
    page = new RdViewPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
