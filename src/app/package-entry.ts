import {TypeRef} from './type-ref';

export class PackageEntry {
  kind: string;
  name: string;
  id: string;

  static fromJson(json: any): PackageEntry {
    return json as PackageEntry;
  }
}
