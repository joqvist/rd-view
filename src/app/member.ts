import {Parameter} from './parameter';
import {Doc} from './doc';
import {TypeRef} from './type-ref';

export class Member {
  name: string;
  type: TypeRef;
  doc: Doc;
  parameters: Parameter[];
  throws: TypeRef[];

  static fromJson(json: any): Member {
    var params: Parameter[] = [];
    var doc: Doc = undefined;
    if (json.doc) {
      doc = Doc.fromJson(json.doc);
    }
    if (json.params) {
      params = json.params as Parameter[];
    }
    var throws: TypeRef[] = undefined;
    if (json.throws) {
      throws = (json.throws as TypeRef[]).map(TypeRef.fromJson);
    }
    if (json.type) {
      return Object.assign({}, json, {
        type: TypeRef.fromJson(json.type),
        parameters: params.map(param => Parameter.fromJson(param)),
        doc: doc,
        throws: throws,
      });
    } else {
      return Object.assign({}, json, {
        parameters: params.map(param => Parameter.fromJson(param)),
        doc: doc,
        throws: throws,
      });
    }
  }
}
