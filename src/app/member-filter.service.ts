import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MemberFilterService {
  private filter = new Subject<string>();

  filter$ = this.filter.asObservable();

  constructor() { }

  setFilter(filter: string) {
    this.filter.next(filter);
  }
}
