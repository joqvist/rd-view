import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Package } from './package';

@Injectable()
export class PackageService {
  private packageUrl = 'data/packages.json';

  constructor(private http: Http) { }

  getPackages(): Promise<Package[]> {
    return this.http.get(this.packageUrl)
      .toPromise()
      .then(response => (response.json().data as Package[]).map(pkg => Package.fromJson(pkg)))
      .catch(res => Promise.reject(`Failed to load packages: ${res}`));
  }
}
