import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class SelectionService {
  private selection = new Subject<string>();

  selection$ = this.selection.asObservable();

  select(id: string) {
    this.selection.next(id);
  }
}
