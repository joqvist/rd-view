export class Doc {
  ast: string;
  ragFile: string;
  line: number;
  description: string;
  apilevel: string;
  params: string[]

  paramDesc(name: string): string {
    if (this.params) {
      for (var i = 0; i < this.params.length; i++) {
        var param = this.params[i];
        var index = param.indexOf(' ');
        if (index >= 0 && param.substring(0, index) === name) {
          return ' : ' + param.substring(index + 1);
        }
      }
    }
    return '';
  }

  static fromJson(json: any): Doc {
    var obj = Object.create(Doc.prototype);
    return Object.assign(obj, json);
  }
}
