import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Package } from './package';
import { Type } from './type';
import { TypeService } from './type.service';
import { MemberFilterService } from './member-filter.service';
import { SelectionService } from './selection.service';
import { Member } from './member';
import { InheritedMembers } from './inherited-members';
import {Doc} from './doc';
import { Parameter } from './parameter';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'type-details',
  styleUrls: ['./type-details.component.css'],
  templateUrl: './type-details.component.html',
  providers: [
    TypeService,
    MemberFilterService,
  ],
})
export class TypeDetailsComponent  implements OnInit {
  type: Type = Object.create(Type.prototype);
  filter: string = '';

  constructor(private typeService: TypeService,
      private memberFilterService: MemberFilterService,
      private route: ActivatedRoute,
      private location: Location,
      private selectionService: SelectionService) {
    memberFilterService.filter$.subscribe(filter => this.filter = filter);
  }

  ngOnInit() {
    this.route.params.switchMap((params: Params) => {
      return this.typeService.getType(params['id'])
    })
    .subscribe(type => {
      this.selectionService.select(type.id);
      this.type = type;
    });
  }

  declaredAt(doc: Doc): string {
    return `${doc.ragFile}:${doc.line}`;
  }

  paramDesc(doc: Doc, name: string): string {
    if (doc) {
      return doc.paramDesc(name);
    }
    return '';
  }

  filteredMembers(kind: string): boolean {
    if (this.type.groups && this.type.groups[kind]) {
      var filter = this.filter.toLowerCase();
      var filtered = this.type.groups[kind].filter(item =>
          item.name.toLowerCase().indexOf(filter) >= 0)
      return filtered.length > 0;
    }
    return false;
  }

  inheritedMembers(inherited: InheritedMembers): boolean {
    var filter = this.filter.toLowerCase();
    var filtered = inherited.members.filter(item => item.toLowerCase().indexOf(filter) >= 0)
    return filtered.length > 0;
  }

  clearFilter() {
    this.filter = '';
  }
}

