import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Type } from './type';

@Injectable()
export class TypeService {

  constructor(private http: Http) { }

  getType(id: string): Promise<Type> {
    const typeUrl = `data/${id}.json`;
    return this.http.get(typeUrl)
        .toPromise()
        .then(response => Type.fromJson(response.json().data))
        .catch(res => Promise.reject(`Failed to load type: ${id}: ${res}.`));
  }
}
