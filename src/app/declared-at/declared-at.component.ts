import { Component, OnInit, Input } from '@angular/core';

import {Doc} from '../doc';

@Component({
  selector: 'declared-at',
  template: `
    Declared at <a [routerLink]="['/source', filename, line]">{{filepath}}:{{line}}.</a>
  `,
})
export class DeclaredAtComponent implements OnInit {

  @Input() doc: Doc;
  filename: string;
  filepath: string;
  line: string;

  constructor() { }

  ngOnInit() {
    this.filepath = this.doc.ragFile;
    this.filename = this.filepath.replace(/\/|\\/g, '_');
    this.line = String(this.doc.line);
  }

}
