export class TypeRef {
  name: string;
  id: string;
  args: TypeRef[];

  static fromJson(json: any): TypeRef {
    var args: TypeRef[] = undefined;
    if (json.a) {
      args = (json.a as TypeRef[]).map(TypeRef.fromJson);
    }
    if (json.u) {
      // User type.
      if (json.i) {
        return {
          name: json.u,
          id: TypeRef.typeId(json.u, json.i),
          args: args,
        };
      } else {
        return {
          name: json.u,
          id: TypeRef.simpleName(json.u),
          args: args,
        };
      }
    } else {
      // Library or built-in type.
      return {
        name: json.n,
        id: undefined,
        args: args,
      };
    }
  }

  // Remove array and type args.
  static simpleName(name: string): string {
    return TypeRef.strip('<', TypeRef.strip('[', name));
  }

  static strip(tok: string, name: string): string {
    const index = name.indexOf(tok);
    if (index < 0) {
      return name;
    } else {
      var simple = name.substring(0, index);
      return simple;
    }
  }

  static typeId(name: string, idPattern: string): string {
    return idPattern.replace('%', TypeRef.simpleName(name));
  }
}
